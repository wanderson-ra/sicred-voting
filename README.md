[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sicred-voting&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=sicred-voting)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sicred-voting&metric=coverage)](https://sonarcloud.io/summary/new_code?id=sicred-voting)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=sicred-voting&metric=bugs)](https://sonarcloud.io/summary/new_code?id=sicred-voting)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=sicred-voting&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=sicred-voting)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=sicred-voting&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=sicred-voting)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=sicred-voting&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=sicred-voting)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=sicred-voting&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=sicred-voting)

# Sicred Voting



## Informações Técnicas
* O sistema foi criado usando **Java** (openjdk versão **"11.0.8" 2020-07-14**).
* **Spring Boot** foi usado
* **Mongo DB** foi usado
* **Feign Client** foi usado para fazer integração com o sistema externo.
* **Spring Data Mongo DB** foi usado
* O sistema foi desenvolvido com **[TDD](https://pt.wikipedia.org/wiki/Test_Driven_Development)**
* O sistema foi desenvolvido usando **[Clean Architecture](https://stackoverflow.com/tags/clean-architecture/info)**
* O sistema usa **[Lombok](https://projectlombok.org/)**. Para o desenvolvimento é necessário instalar o plug-in no IDE (caso contrário, haverá erros de compilação).
* **Swagger** Foi usado para documentar a API. Para acessar, use o endereço: https://sicred-voting.herokuapp.com/swagger-ui.html
* Mais de 80% de cobertura de testes de unidade.

## Ferramentas Usadas nos Testes
* Mockito
* Faker


## Ferramentas Usadas para CI/CD e Qualidade
* **GitLabCI**
* **[Heroku](https://sicred-voting.herokuapp.com/swagger-ui.html)**
* **[Sonar Cloud](https://sonarcloud.io/project/overview?id=sicred-voting)**

#### Instalar dependências e executar testes (necessita maven instalado)

maven:

```
 mvn clean install
```

## Modos de Execução


#### 1 - Executar projeto local via linha de comando com docker-compose (Na pasta raiz, necessita docker e docker-compose instalados)

Porta default: 8080

Swagger: http://localhost:8080/swagger-ui.html

Passo 1:

```
docker-compose build
```

Passo 2:

```
docker-compose up 
```


#### 2 - Executar projeto local via linha de comando (Na pasta raiz, necessita maven instalado e instância do mongo rodando)

Porta default: 8000

Swagger: http://localhost:8000/swagger-ui.html


maven:

```
mvn spring-boot:run -Dspring.profiles.active=local
```




